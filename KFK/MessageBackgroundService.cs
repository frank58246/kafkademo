﻿using Confluent.Kafka;

namespace KFK
{
    public class MessageBackgroundService : BackgroundService
    {
        private readonly string topic = "simpletalk_topic";
        private IConsumer<Ignore, string> builder;
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var conf = new ConsumerConfig
            {
                GroupId = "st_consumer_group",
                BootstrapServers = "localhost:9092",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };
            builder = new ConsumerBuilder<Ignore, string>(conf).Build();
            builder.Subscribe(topic);

            var cancelToken = new CancellationTokenSource();
            Task.Run(() =>
            {
                while (true) {
                    var consumer = builder.Consume(cancelToken.Token);
                    Console.WriteLine($"Message: {consumer.Message.Value} received from {consumer.TopicPartitionOffset}");

                }

            });
        
            
            return Task.CompletedTask;
        }
    }
}